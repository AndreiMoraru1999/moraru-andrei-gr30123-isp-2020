package moraru.andrei.lab7.ex4;
import java.util.*;
import java.io.*;

public class Serialization
{
    class car implements Serializable
    {
        char model;
        double pret;


        public car(char model, double pret)
        {
            this.model = model;
            this.pret = pret;
        }

        @Override
        public String toString()
        {
            return "[masina " + model + " " + pret + "]";
        }

        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
        {
            model = in.readChar();
            pret=in.readDouble();
        }
    }

    void save(String fileName) {
        try
        {
            ObjectOutputStream o =
                    new ObjectOutputStream(
                            new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Masina salvata in fisier");
        }
        catch (IOException e)
        {
            System.err.println("Masina nu poate fi salvata in fisier");
            e.printStackTrace();
        }
    }

}


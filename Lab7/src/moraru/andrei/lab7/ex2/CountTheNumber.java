package moraru.andrei.lab7.ex2;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class CountTheNumber {
    public static void main(String[] args)throws IOException
    {

        String fileName = "Data.txt";
        String line = "";
        Scanner scanner = new Scanner(new FileReader(fileName));
        try {
            int counter = 0;
            while ( scanner.hasNextLine() ){
                line = scanner.nextLine();

                for( int i=0; i<line.length(); i++ ) {
                    if( line.charAt(i) == 'e' ) {
                        counter++;

                    }


                }

                System.out.println(counter);
            }
        }
        finally {

            scanner.close();


        }}}
package moraru.andrei.lab7.ex3;

import java.util.Scanner;
import java.io.*;


public class Cipher
{
    String fileName = "Data.txt";
    String cipher(String msg, int shift){
        String s = "";
        int len = msg.length();
        for(int x = 0; x < len; x++){
            char c = (char)(msg.charAt(x) + shift);
            if (c > 'z')
                s += (char)(msg.charAt(x) - (26-shift));
            else
                s += (char)(msg.charAt(x) + shift);
        }
        return s;
    }

}

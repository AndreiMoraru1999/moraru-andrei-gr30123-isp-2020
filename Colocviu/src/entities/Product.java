package entities;

import actions.SellProduct;

class ProductDetails
{
    public int  id;
    public String name;
    public int price;


    ProductDetails(int i,String n, int p)
    {

        id=i;
        name=n;
        price=p;
    }

    public ProductDetails() {

    }
}

public class Product extends ProductDetails implements SellProduct
{
    public boolean forSale;
    public int categoryId;

    public Product(int i, String n, int p, boolean f, int c)
    {
        super(i, n, p);
        forSale=f;
        categoryId=c;
    }

    public Product()
    {
        super();

    }


    public void markForSale()
    {
        forSale=true;
    }

    public void retireFromSale()
    {
        forSale=false;
    }

    @Override
    public void markForSale(Product P1) {

    }
}

class Book extends Product
{

    Book(int i, String n, int p, boolean f, int c)
    {
        super(i, n, p, f, c);
    }

    public void markForSale()
    {
        forSale=true;
    }

    public void retireFromSale()
    {
        forSale=false;
    }
}

class Toys extends Product
{

    Toys(int i, String n, int p, boolean f, int c)
    {
        super(i, n, p, f, c);
    }

    public void markForSale()
    {
        forSale=true;
    }

    public void retireFromSale()
    {
        forSale=false;
    }
}


package categories;

import entities.Product;

import java.awt.print.Book;

class Site
{
    public String name;
    public String url;

    Site(String na,String u)
    {
        name=na;
        url=u;
    }

    public Site()
    {

    }

    public void initCategories()
    {
        Category C1 = new Category();

        System.out.println(url);
        System.out.println(name);
        System.out.println(C1);

    }
}


public class Category extends Site
{
    public int id;
    public String name;

   public Category(int i, String n,String na, String u)
    {
        super(na,u);
        id=i;
        name=n;
    }

    public Category()
    {

    }


    public void addProduct(Product p)
    {
        Product P1= new Product();
        P1.markForSale();
    }

    public void removeProduct(Product p)
    {
        Product P1= new Product();
        P1.retireFromSale();
    }

    public void updateProduct(Product p)
    {
      Product P1=new Product();
      P1=p;
    }

    public void getProduct(int id)
    {
        System.out.println(this.id);
    }

}

abstract class BookCategory extends Category
{

    BookCategory(int i, String n,String na,String u)
    {
        super(i, n, na, u);
    }

    public void addProduct(Product p)
    {
        Product P1= new Product();
        P1.markForSale();
    }

    public void removeProduct(Product p)
    {
        Product P1= new Product();
        P1.retireFromSale();
    }

    public void updateProduct(Product p)
    {

    }

    public void getProduct(int id)
    {
        System.out.println(this.id);
    }
}

class ToyCategory extends Category
{

    ToyCategory(int i, String n,String na, String u)
    {
        super(i, n,na,u);
    }

    public void addProduct(Product p)
    {
        Product P1= new Product();
        P1.markForSale();
    }

    public void removeProduct(Product p)
    {
        Product P1= new Product();
        P1.retireFromSale();
    }

    public void updateProduct(Product p)
    {

    }

    public void getProduct(int id)
    {
        System.out.println(this.id);
    }
}

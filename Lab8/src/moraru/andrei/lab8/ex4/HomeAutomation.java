package moraru.andrei.lab8.ex4;

import java.util.Random;

public class HomeAutomation {


    public static void main(String[] args){

        //test using an annonimous inner class
        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
            }
            protected void controllStep(){
                System.out.println("Control step executed");
            }
        };
        h.simulate();
    }
}




class controlUnit
    {
        boolean call;
        controlUnit(boolean call)
        {
            this.call=call;
        }

        boolean Calling()
        {
            return call;
        }

        @Override
        public String toString() {
            return "controlUnit{" + "call=" + call + '}';
        }
    }


class alarmUnit
    {
        boolean alarm;
        alarmUnit(boolean alarm)
        {
            this.alarm=alarm;
        }
        boolean isAlarm()
        {
            return alarm;
        }

        @Override
        public String toString() {
            return "AlarmUnit{" + "alarm=" + alarm + '}';
        }
    }


abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event);
    protected abstract void controllStep();

    private Event getHomeEvent(){
        //randomly generate a new event;
        int k = r.nextInt(100);
        if(k<30)
            return new NoEvent();
        else if(k<60)
            return new FireEvent(r.nextBoolean(), r.nextBoolean());
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate(){
        int k = 0;
        while(k <SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }

}

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }

}

class TemperatureEvent extends Event{

    private int vlaue;
    private int tmax;
    private int tmin;
    boolean heat;
    boolean cool;

    TemperatureEvent(boolean heat, boolean cool)
    {
        super(EventType.FIRE.TEMPERATURE);
        this.heat= heat;
        this.cool=cool;

    }

    int cooling(int t)
    {
        t=vlaue;
        while(t> tmax){ t--;}
        return t;
    }


    int heating(int t)
    {
        t=vlaue;
        while(t< tmin){ t++;}
        return t;
    }
    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    int getVlaue() {
        return vlaue;
    }

    void TempSensor()
    {
        if(vlaue < tmax && vlaue > tmin)
        {
            System.out.println("TemperatureEvent{" + "vlaue=" + vlaue + '}');
        }

        else
        {
            if(vlaue < tmin) {
                heating(vlaue);
            }

            if(vlaue > tmax) {
                cooling(vlaue);
            }
        }
    }

    @Override
    public String toString() {
        return "TemperatureEvent{" + "vlaue=" + vlaue + '}';
    }

}

class FireEvent extends Event {

    private boolean smoke;
    private boolean alarm;

    FireEvent(boolean smoke, boolean alarm) {
        super(EventType.FIRE);
        this.smoke = smoke;
        this.alarm=alarm;
    }

    boolean isSmoke() {
        return smoke;
    }

    void FireSensor()
    {
        if(smoke)
        {
            alarm=true;
        }
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }

}

class NoEvent extends Event{

    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}

enum EventType {
    TEMPERATURE, FIRE, NONE;
}
package moraru.andrei.lab10.ex6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer {

    private static long start;

    public Chronometer() {
        start = 0;
    }

    public static void start() {
        if(start == 0)
            start = System.currentTimeMillis();
    }

    public static void stop()
    {
        Chronometer.stop();
    }


    public double count() {
        if(start == 0) { return 0; }

        double time = (System.currentTimeMillis() - start) / 1000.0;
        start = 0;
        return time;
    }

    class Stopwatch {
        JFrame frame;
        JButton button1;
        JButton button2;
        ButtonHandler buttonHandler;
        Timer timer;

        Stopwatch(){
            frame = new JFrame("Stopwatch");

            button1 = new JButton("Start");
            button2 = new JButton("Stop");

            buttonHandler = new ButtonHandler(this);
            button1.addActionListener(buttonHandler);
            button2.addActionListener(buttonHandler);

            timer = new Timer(1000, buttonHandler);
        }

        public void main(String[] arg) {
            new Stopwatch();
        }

    }

    class ButtonHandler implements ActionListener {
        Stopwatch count;

        public ButtonHandler(Stopwatch count) {
            this.count = count;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("Start"))
            {
                Chronometer.start();
            }

            else if (e.getActionCommand().equals("Stop"))
            {
                Chronometer.stop();
            }

        }
    }
    }
package moraru.andrei.lab10.ex4;
import java.util.Scanner;

public class Robot {
    Scanner in = new Scanner(System.in);

    public void run (){
        int k;
        k=in.nextInt();
        int collide=0;
        Thread t = Thread.currentThread();
        for(int i=0;i<100;i++){
            if(k>=1) i=i+k;
            if(i==collide) Thread.interrupted();
            collide=i;
            try {
                Thread.sleep((int)(Math.random() * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(t.getName() + " job finalised.");
    }
    public static void main(String args[])
    {
        Robot R1= new Robot();
        Robot R2= new Robot();
        Robot R3= new Robot();
        Robot R4= new Robot();
        Robot R5= new Robot();
        Robot R6= new Robot();
        Robot R7= new Robot();
        Robot R8= new Robot();
        Robot R9= new Robot();
        Robot R10= new Robot();

        R1.run();
        R2.run();
        R3.run();
        R4.run();
        R5.run();
        R6.run();
        R7.run();
        R8.run();
        R9.run();
        R10.run();

    }

}
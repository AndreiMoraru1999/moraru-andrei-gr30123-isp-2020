package moraru.andrei.lab4.ex5;

import moraru.andrei.lab4.ex1.Circle;

public class Cylinder
{
  double height;
  double radius;
  Cylinder()
  {
      height = 1.0;
  }
  Cylinder(double a)
  {
      radius=a;
  }
  Cylinder(double x, double y)
  {
      height=x;
      radius=y;
  }

  public void getHeight()
  {
      System.out.println(this.height);
  }

  public void getVolume()
  {
      double v=3.14*radius*radius*height;
      System.out.println("Volume is " + v);
  }

  public void getAreaT()
  {
      double a=2*3.14*radius*(height+radius);
      System.out.println("Area is "+ a);
  }
}


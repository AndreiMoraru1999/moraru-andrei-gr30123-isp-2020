package moraru.andrei.lab4.ex4;

import moraru.andrei.lab4.ex2.Author;

public class Book2
{
    private String Name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    Book2(String N, Author[] a, double p)
    {
        Name=N;
        authors=a;
        price=p;
    }

    Book2(String N, Author[] a, double p, int q)
    {
        Name=N;
        authors=a;
        price=p;
        qtyInStock=q;
    }

    public void getName()
    {
        System.out.println(this.Name);
    }

    public void getAuthors()
    {
        System.out.println(this.authors);
    }

    public void getPrice()
    {
        System.out.println(this.price);
    }

    public void getQtyInStock()
    {
        System.out.println(this.qtyInStock);
    }

    public void setPrice(double price)
    {
        this.price=price;
    }

    public void setQtyInStock(int qtyInStock)
    {
        this.qtyInStock=qtyInStock;
    }

    public void toooString()
    {
        System.out.println(Name);
    }

    public void printAuthors()
    {
        System.out.println(authors);
    }
}

package moraru.andrei.lab4.ex3;

import moraru.andrei.lab4.ex2.Author;

public class Book
{
    private String Name;
    private Author author;
    private double price;
    private int qtyInStock;

    Book(String N, Author a, double p)
    {
    Name=N;
    author=a;
    price=p;
    }

    Book(String N, Author a, double p, int q)
    {
        Name=N;
        author=a;
        price=p;
        qtyInStock=q;
    }

    public void getName()
    {
        System.out.println(this.Name);
    }

    public void getAuthor()
    {
        System.out.println(this.author);
    }

    public void getPrice()
    {
        System.out.println(this.price);
    }

    public void getQtyInStock()
    {
        System.out.println(this.qtyInStock);
    }

    public void setPrice(double price)
    {
        this.price=price;
    }

    public void setQtyInStock(int qtyInStock)
    {
        this.qtyInStock=qtyInStock;
    }

    public void toooString()
    {
        System.out.println(Name);
    }
}

package moraru.andrei.lab4.ex2;

public class Author
{
    private String name;
    private String email;
    private char gender;
    Author(String n,String e,char g)
    {
        name=n;
        email=e;
        gender=g;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public void getName()
    {
        System.out.println(this.name);
    }
    public void getEmail()
    {
        System.out.println(this.email);
    }
    public void getGender()
    {
        System.out.println(this.gender);
    }
    public void toostring()
    {
        System.out.println("My "+name + " ("+gender+")"+" at= "+email);
    }
}

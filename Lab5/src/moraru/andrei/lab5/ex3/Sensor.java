package moraru.andrei.lab5.ex3;

public class Sensor
{
    public String location;

    public void readValue(int v)
    {
        int value=v;
    }

    public void getLocation()
    {
        System.out.println(this.location);
    }
}

class TemperatureSensor extends Sensor
{
   public int temperature;

   TemperatureSensor()
   {
       temperature=27;
   }
}

class LightSensor extends Sensor
{
    public int lightValue;
    LightSensor()
    {
        lightValue=20;
    }
    public void getLight()
    {
        System.out.println(this.lightValue)
    }
}

class Controller extends TemperatureSensor
{
    public void control()
    {
        System.out.println("Value of temperature : " + temperature );
}
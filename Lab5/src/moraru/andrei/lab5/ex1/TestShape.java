package moraru.andrei.lab5.ex1;

class Shape
{
    public String color;
    public boolean filled;
    Shape()
    {
        color="green";
        filled=true;
    }
    Shape(String c, boolean f)
    {
        color=c;
        filled=f;
    }
    public void getColor()
    {
        System.out.println(this.color);
    }

    public void Filled()
    {
        System.out.println(this.filled);
    }

    public void setColor()
    {
        this.color=color;
    }

    public void setFilled()
    {
        this.filled=filled;
    }

    public void afisare()
    {
        System.out.println("A shape with color of " +color "and" filled);
    }
}

class Circle extends Shape
{
    double radius;
    Circle()
    {
        radius=1.0;
    }
    Circle(double r)
    {
        radius = r;
    }
    Circle(double r, String c, boolean f)
    {
        radius = r;
        color = c;
        filled = f;
    }

    public void getRadius()
    {
        System.out.println("Radius is "+radius);
    }
    public void getArea()
    {
        double a=3.14*radius*radius;
        System.out.println("Area is "+a);
    }

    public void setRadius()
    {
        this.radius=radius;
    }
    public void getPerimeter()
    {
        double p=2*3.13*radius;
        System.out.println("Perimeter is "+ p);
    }

    public void AfisareCerc()
    {
        System.out.println("A circle with radius = " +radius + "which is a subclass of Shape");
    }
}

class Rectangle extends Shape
{
    double width;
    double length;

    Rectangle()
    {
        width=1.0;
        length=1.0;
    }
    Rectangle(double w,double l)
    {
        width = w;
        length=l;
    }
    Rectangle(double w, double l, String c, boolean f)
    {
        width = w;
        length=w;
        color = c;
        filled = f;
    }

    public void getWidth()
    {
        System.out.println("Width is "+width);
    }

    public void getLength()
    {
        System.out.println("Length is "+length);
    }

    public void getArea()
    {
        double a=width*length;
        System.out.println("Area is "+a);
    }

    public void setWidth()
    {
        this.width=width;
    }

    public void setLength()
    {
        this.length=length;
    }
    public void getPerimeter()
    {
        double p=2*width+2*length;
        System.out.println("Perimeter is "+ p);
    }

    public void AfisareDreptunghi()
    {
        System.out.println("A rectangle with width = " +width  +"and length = " + length + " which is a subclass of Shape");
    }
}

class Square extends Rectangle
{
    double side;

    double radius;

    Square()
    {
        radius=1.0;
    }
    Square(double s)
    {
        side = s;
    }
    Square (double s, String c, boolean f)
    {
        side = s;
        color = c;
        filled = f;
    }

    public void getSide()
    {
        System.out.println("Side is "+side);
    }
    public void setSide()
    {
        this.side=side;
    }

    public void AfisarePatrat()
    {
        System.out.println("A square with width = " +side + "which is a subclass of Shape");
    }

}

public class TestShape
{

    Rectangle R1=new Rectangle();
    R1.getArea();
    R1.getPerimeter();
    R1.AfisareDreptunghi();

    Circle C1=new Circle();
    C1.getArea();
    C1.getPermeter();
    C1.AfisareCerc();

    Square S1=new Square();
    S1.getArea();
    S1.getPermeter();
    S1.AfisarePatrat();


}


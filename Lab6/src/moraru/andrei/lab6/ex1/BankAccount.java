package moraru.andrei.lab6.ex1;

public class BankAccount {
    public String owner;
    public double balance;

    BankAccount()
    {
        owner="Andrei Moraru";
        balance= 100;
    }

    BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw() {
        double amount = 25;
        if (amount < balance) {
            balance = balance - amount;
        }
    }

    public void deposit() {
        double amount = 10;
        balance = balance + amount;
    }

    public boolean equals(Object obj)
    {
        if(obj instanceof BankAccount)
        {
            BankAccount b= (BankAccount)obj;
            return balance==b.balance;
        }
        return false;
    }

    public double hashcode()
    {
        return balance + owner.hashCode();
    }

}

public static void main(String[] args)

{
    BankAccount b1 = new BankAccount("Mihai", 50);
    BankAccount b2 = new BankAccount("Elena", 70);

    if (b1.equals(b2))
        System.out.println(b1 + " and " + b2 + " are equals");
    else
        System.out.println(b1 + " and " + b2 + " are NOT equals");
}




package moraru.andrei.lab6.ex4;

import java.util.ArrayList;
import java.util.List;

class Dictionary
{
    public String name;
    public String description;

    Dictionary(String name, String description)
    {
        this.name=name;
        this.description=description;
    }

    public void addWord(String w, String d)
    {
        name=w;
        description=d;
    }
    public void getDefinition()
    {
        System.out.println(this.description);
    }

    public void getAllWords()
    {
        List z1= new ArrayList();
        for(int i=0; i<z1.size(); i++)
        {
            String name = (String) z1.get(i);
            System.out.println(this.name);
            String description = (String) z1.get(i);
            System.out.println(this.description);
        }
    }

    public void getAllDefinitions()
    {
        List z1= new ArrayList();
        for(int i=0; i<z1.size(); i++)
        {
            System.out.println(this.description);
        }
    }

}


public class ConsoleMenu
{
    Dictionary d1= new Dictionary("albina", " Insectă din familia apidelor, cu picioarele posterioare adaptate pentru strângerea polenului și cu abdomenul prevăzut cu un ac veninos, care trăiește în familii și produce miere și ceară");
    Dictionary d2= new Dictionary("avion", "Vehicul aerian mai greu decât aerul, prevăzut cu mari suprafețe plane în formă de aripi și care se deplasează cu ajutorul elicei și al motoarelor sau reactoarelor");

    public Dictionary getD2()
    {
        return d2;
    }

    public Dictionary getD1()
    {
        return d1;
    }
}

package moraru.andrei.lab11.ex2;

import javax.swing.*;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Observable;
        import java.util.Observer;

class Product extends Observable
{


    private String name;
    private int quantity;
    private double price;

    List<Product> products = new ArrayList<Product>();

    Product() {}

    Product(String name, int quantity, double price)
    {

        this.name = name;
        this.price = price;
        this.quantity = quantity;

    }

    public void AddNewProduct(String name, int quantity, double price)
    {

        Product product = new Product(name, quantity, price);
        products.add(product);
        this.setChanged();
        this.notifyObservers(name);

    }


    public void viewAvailableProduct()
    {

        for (Product product : products) {
            System.out.println(product.toString());
        }

    }

    public void deleteProduct(String name)
    {
        for (Product product : products)
        {
            if (product.name.equals(name))
            {
                products.remove(product);
                setChanged();
                notifyObservers("Deleted "+name);
                break;
            }
        }
    }

    public void changeProductAvailableQuantity(String name, int quantity)
    {
        for (Product product : products)
        {
            if (product.name.equals(name))
            {
                product.quantity = quantity;
                break;
            }
        }

    }


    public String toString()
    {
        return String.format("Name:%s  ,quantity:%d   ,price:%f \n",name,quantity,price);
    }
}
class ViewStock extends JFrame implements Observer
{

    Product proView;
    JTextArea textPro;
    static JTextArea textProObservers;

    JButton AddNewProduct;
    JButton viewAvailableProducts;
    JButton deleteProduct;
    JButton changeProductAvailableQuantity;

    JTextField nume;
    JTextField quantity;
    JTextField pret;

    JLabel numeP;
    JLabel quantityP;
    JLabel pretP;


    ViewStock(Product proView)
    {

        this.proView = proView;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Control products");
        init();
        setVisible(true);
        setSize(720, 500);

    }

    void init()
    {
        int widthPro = 100;
        int heightPro = 30;
        setLayout(null);


        textPro = new JTextArea();
        textPro.setBounds(10, 10, 350, 200);

        for (Product product : proView.products)
        {

            textPro.append(product.toString());

        }
        add(textPro);

        textProObservers = new JTextArea();
        textProObservers.setBounds(10, 240, 350, 200);

        add(textProObservers);


        AddNewProduct = new JButton("add Product");
        AddNewProduct.setBounds(380, 20, widthPro, heightPro);
        AddNewProduct.addActionListener(new Button());

        viewAvailableProducts = new JButton("viewProducts");
        viewAvailableProducts.setBounds(380, 70, widthPro, heightPro);
        viewAvailableProducts.addActionListener(new Button());

        deleteProduct = new JButton("deleteProd");
        deleteProduct.setBounds(380, 120, widthPro, heightPro);
        deleteProduct.addActionListener(new Button());

        changeProductAvailableQuantity = new JButton("changeQuant");
        changeProductAvailableQuantity.setBounds(380, 170, widthPro, heightPro);
        changeProductAvailableQuantity.addActionListener(new Button());

        numeP = new JLabel("Nume Produs");
        numeP.setBounds(500, 50, 90, 20);

        nume = new JTextField();
        nume.setBounds(600, 50, 90, 20);

        quantityP = new JLabel("Cantitate Produs");
        quantityP.setBounds(500, 80, 90, 20);

        quantity = new JTextField();
        quantity.setBounds(600, 80, 90, 20);

        pretP = new JLabel("Pret");
        pretP.setBounds(500, 110, 90, 20);

        pret = new JTextField();
        pret.setBounds(600, 110, 90, 20);

        add(numeP);
        add(quantityP);
        add(nume);
        add(quantity);
        add(pretP);
        add(pret);
        add(AddNewProduct);
        add(viewAvailableProducts);
        add(deleteProduct);
        add(changeProductAvailableQuantity);
    }

    class Button implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (AddNewProduct.getModel().isArmed())
            {

                try {


                    proView.AddNewProduct(nume.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(pret.getText()));
                    textPro.setText(null);


                    for (Product product : proView.products)
                    {

                        textPro.append(product.toString());

                    }
                }
                catch (Exception ee) {}


            }
            if (viewAvailableProducts.getModel().isArmed())
            {

                try {

                    textPro.setText(null);


                    for (Product product : proView.products)
                    {

                        textPro.append(product.toString());

                    }


                }
                catch (Exception ee) {}
            }

            if (deleteProduct.getModel().isArmed()) {

                try {

                    proView.deleteProduct(nume.getText());
                    textPro.setText(null);


                    for (Product product : proView.products) {

                        textPro.append(product.toString());

                    }


                } catch (Exception ee) {
                }
            }

            if (changeProductAvailableQuantity.getModel().isArmed())
            {

                try {

                    proView.changeProductAvailableQuantity(nume.getText(), Integer.parseInt(quantity.getText()));
                    textPro.setText(null);

                    for (Product product : proView.products)
                    {

                        textPro.append(product.toString());

                    }


                } catch (Exception ee) {}
            }


        }
    }

        @Override
        public void update(Observable o, Object arg)
        {
            textProObservers.append("Produsul a fost modificat: " + arg + "\n");
        }
    }

public class Management
{
    public static void main(String[] args)
    {

        Product product=new Product();
        product.AddNewProduct("Pompa",23,239);
        product.AddNewProduct("Fierastrau",30,199);
        product.AddNewProduct("Motocoasa",15,379);
        ViewStock viewStock=new ViewStock(product);
        product.addObserver(viewStock);




    }
}

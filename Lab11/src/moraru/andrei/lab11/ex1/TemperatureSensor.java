package moraru.andrei.lab11.ex1;

import javax.swing.*;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Observable;
        import java.util.Random;

public class TemperatureSensor extends JFrame

{

    JTextField text;
    JTextArea display;
    JButton x;

    ChangeData changeData = new ChangeData();

    TemperatureSensor()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("SensorMonitoring");
        init();
        setVisible(true);
        setSize(300, 700);

    }

    public void init()
    {
        setLayout(null);
        int height = 20;
        int width1 = 30;
        int width2 = 250;

        text = new JTextField();
        text.setBounds(120, 200, width1, height);

        display = new JTextArea();
        display.setBounds(10, 500, width2, height);

        x = new JButton("Pause");
        x.setBounds(100, 550, 70, height);
        x.addActionListener(new Button());

        add(x);
        add(text);
        add(display);
    }

    class Button implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {

            try
            {
                changeData.setPause();
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    class Observable
    {

        private List<Observer> observerList = new ArrayList<Observer>();

        public void register(Observer obs)
        {
            observerList.add(obs);
        }

        public void changeEvent(String event)
        {
            notifyObservers(event);
        }

        public void notifyObservers(String event)
        {
            for (Observer obs : observerList)
                obs.update(event);
        }

    }

    interface Observer
    {
        public abstract void update(String event);
    }

    class VerifyTemperatureChangeAlert implements Observer
    {
        public void update(String event)
        {
            display.setText("temp value has changed: " + event);
        }
    }

    public void start()
    {
        TemperatureTest th1 = new TemperatureTest();
        th1.start();
    }

    class TemperatureTest extends Thread
    {


        public void run()
        {

            while (true)
            {

                try
                {
                    sleep(1000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    changeData.changeValueTemp();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

            }
        }

    }

    class ChangeData
    {

        VerifyTemperatureChangeAlert verifyTemperatureChangeAlert;
        Temperature t;
        Observable Temp;
        int nrTests = 1;
        boolean pause = false;

        ChangeData()
        {

            verifyTemperatureChangeAlert = new VerifyTemperatureChangeAlert();
            t = new Temperature();
            Temp = new Observable();
            Temp.register(verifyTemperatureChangeAlert);

        }


        synchronized void changeValueTemp() throws InterruptedException
        {
            if (!pause)
            {
                text.setText(String.valueOf(t.getValue()));
                Temp.notifyObservers(String.format("TEST %d", nrTests++));
                t = new Temperature();
            }

            else {
                wait();
            }
        }

        synchronized public void setPause() throws InterruptedException
        {
            {
                if (!pause)
                {
                    pause = true;

                } else
                    {
                    pause = false;
                    notifyAll();
                }
            }
        }

    }

    class Temperature
    {

        double value;

        Temperature()
        {
            Random in = new Random();
            value = in.nextInt(40);
        }

        double getValue()
        {
            return value;
        }
    }


    public static void main(String[] args)
    {
        new TemperatureSensor().start();
    }
}
package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

class Problema extends JFrame {

    JTextArea zonaText;
    JButton button;
    Random x=new Random();

    Problema(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Problema");
        setLayout(null);

        int height=100;
        int width=200;

        zonaText=new JTextArea();
        zonaText.setBounds(10,10,width,height);

        button=new JButton("Press");
        button.setBounds(100,250,70,20);
        button.addActionListener(new Buton());

        add(zonaText);add(button);

        setVisible(true);
        setSize(300,400);
    }

    class Buton implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {

            zonaText.append(String.valueOf(x.nextInt(10)));
            zonaText.append("\n");


        }
    }

    public static void main(String[] args) {
        new Problema();
    }
}
package ex1;

public class I extends U implements J
{
    private long t;

    private K d;

    public void t()
    {
        System.out.println("metoda t");
    }

    public void i(J j )
    {
        System.out.println(j);
    }

    I(int t)
    {
        this.t=t;
    }

    public static void main(String[] args) {
        J j ;
        long t=5;
        I i1 = new I(Math.toIntExact(t));


    }
}

interface J
{
    void i(J j);
}

class U
{
    public void p()
    {
        System.out.println("metoda p");
    }

}

class K
{
    L l1 = new L();
}

class L
{
    public void metA()
    {
        System.out.println("metA");
    }
}

class N
{
    private I n1;
}

class S
{
    private K s1;

    public void metB()
    {
        System.out.println("metB");
    }
}
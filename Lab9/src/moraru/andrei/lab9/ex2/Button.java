package moraru.andrei.lab9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class Button extends JFrame {

    HashMap accounts = new HashMap();

    JLabel counter;
    JTextField Counter;
    JTextArea tArea;
    JButton increment;

    Button() {

        accounts.put("counter", "Counter");

        setTitle("Test counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        counter = new JLabel();
        counter.setBounds(10, 50, width, height);

        Counter = new JTextField();
        Counter.setBounds(70, 50, width, height);


        increment = new JButton("increment");
        increment.setBounds(10, 150, width, height);

        increment.addActionListener(new TratareButonIncrement());

        tArea = new JTextArea();
        tArea.setBounds(10, 180, 150, 80);

        add(counter);
        add(Counter);
        add(increment);
        add(tArea);

    }

    public static void main(String[] args) {
        new Button();
    }

    class TratareButonIncrement implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            int c = Button.this.counter.getComponentCount();

            if (Button.this.accounts.containsKey(c)) {
                c++;
                Button.this.tArea.append(String.valueOf(c));
            }

        }
    }
}

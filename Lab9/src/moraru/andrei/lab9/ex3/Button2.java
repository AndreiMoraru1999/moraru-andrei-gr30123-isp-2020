package moraru.andrei.lab9.ex3;

import moraru.andrei.lab9.ex2.Button;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class Button2 extends JFrame {

    HashMap accounts = new HashMap();

    JLabel content;
    JTextField display;
    JTextArea tArea;
    JButton press;

    Button2() {

        accounts.put("content", "display");

        setTitle("Test display");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        content = new JLabel();
        content.setBounds(10, 50, width, height);

        display = new JTextField();
        display.setBounds(70, 50, width, height);


        press = new JButton("press");
        press.setBounds(10, 150, width, height);

        press.addActionListener(new TratareButonIncrement2());

        tArea = new JTextArea();
        tArea.setBounds(10, 180, 150, 80);

        add(content);
        add(display);
        add(press);
        add(tArea);

    }

    public static void main(String[] args) {
        new Button2();
    }

    class TratareButonIncrement2 implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String text = Button2.this.display.getText();

            if (Button2.this.accounts.containsKey(text)) {

                Button2.this.tArea.append(text);
            }

        }
    }
}

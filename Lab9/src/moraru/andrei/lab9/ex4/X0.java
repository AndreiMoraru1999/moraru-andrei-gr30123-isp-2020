package moraru.andrei.lab9.ex4;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class X0 {
    static Scanner in;
    static String[] tabla;
    static String simbol;

    public static void main(String[] args) {
        in = new Scanner(System.in);
        tabla = new String[9];
        simbol = "X";
        String winner = null;
        completareTabla();
        afisareTabla();
        System.out.println("X incepe:");

        while (winner == null) {
            int numInput;
            try {
                numInput = in.nextInt();
                if (!(numInput > 0 && numInput <= 9)) {
                    System.out.println("Invalid input; re-enter slot number:");
                    continue;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input; re-enter slot number:");
                continue;
            }
            if (tabla[numInput-1].equals(String.valueOf(numInput))) {
                tabla[numInput-1] = simbol;
                if (simbol.equals("X")) {
                    simbol = "O";
                } else {
                    simbol = "X";
                }
                afisareTabla();
                winner = checkWinner();
            } else {
                System.out.println("Locul este luat, alegeti alt loc:");
                continue;
            }
        }
        if (winner.equalsIgnoreCase("egalitate")) {
            System.out.println("Este egalitate");
        } else {
            System.out.println( winner + "a castigat");
        }
    }

    static String checkWinner() {
        for (int a = 0; a < 8; a++) {
            String line = null;
            switch (a) {
                case 0:
                    line = tabla[0] + tabla[1] + tabla[2];
                    break;
                case 1:
                    line = tabla[3] + tabla[4] + tabla[5];
                    break;
                case 2:
                    line = tabla[6] + tabla[7] + tabla[8];
                    break;
                case 3:
                    line = tabla[0] + tabla[3] + tabla[6];
                    break;
                case 4:
                    line = tabla[1] + tabla[4] + tabla[7];
                    break;
                case 5:
                    line = tabla[2] + tabla[5] + tabla[8];
                    break;
                case 6:
                    line = tabla[0] + tabla[4] + tabla[8];
                    break;
                case 7:
                    line = tabla[2] + tabla[4] + tabla[6];
                    break;
            }
            if (line.equals("XXX")) {
                return "X";
            } else if (line.equals("OOO")) {
                return "O";
            }
        }

        for (int a = 0; a < 9; a++) {
            if (Arrays.asList(tabla).contains(String.valueOf(a+1))) {
                break;
            }
            else if (a == 8) return "egalitate";
        }

        System.out.println("Randul lui" + simbol +  " alege locul de pe tabla unde vrei sa pui " + simbol + " in:");
        return null;
    }

    static void afisareTabla() {
        System.out.println("/---|---|---\\");
        System.out.println("| " + tabla[0] + " | " + tabla[1] + " | " + tabla[2] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + tabla[3] + " | " + tabla[4] + " | " + tabla[5] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + tabla[6] + " | " + tabla[7] + " | " + tabla[8] + " |");
        System.out.println("/---|---|---\\");
    }

    static void completareTabla() {
        for (int a = 0; a < 9; a++) {
            tabla[a] = String.valueOf(a+1);
        }
    }
}
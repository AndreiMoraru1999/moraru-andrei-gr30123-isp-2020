package moraru.andrei.lab3.ex5;

public class Flower
{
    int petal;
    static int n;
    static int c=1;

    Flower()
    {
        System.out.println("Flower has been created!");
        if(c==1)
        {
            n=c;
            c=0;
        }
        else
        {
            n++;
        }

    }
    public static void afis()
    {
        System.out.println("Nr contructorilor = "+ n);
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }

        Flower.afis();
    }
}
package moraru.andrei.lab3.ex3;

class Author
{
    private String name;
    private String email;
    private char gender;
    Author(String n,String e,char g)
    {
        name=n;
        email=e;
        gender=g;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public void getName()
    {
        System.out.println(this.name);
    }
    public void getEmail()
    {
        System.out.println(this.email);
    }
    public void getGender()
    {
        System.out.println(this.gender);
    }
    public void toostring()
    {
        System.out.println("My "+name + " ("+gender+")"+" at= "+email);
    }
}


public class TestAuthor
{
    public static void main(String args[])
    {
        Author test=new Author("Moraru Andrei","andreimoraru1999@yahoo.com",'m');
        test.toostring();
        test.getName();
        test.getEmail();
        test.getGender();
        test.setEmail("randomemail@yahoo.com");
    }
}
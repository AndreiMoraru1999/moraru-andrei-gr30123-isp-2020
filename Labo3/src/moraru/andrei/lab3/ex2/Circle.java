package moraru.andrei.lab3.ex2;

class CircleModel
{
    double radius;
    String color;

    CircleModel()
    {
        radius=1.0;
        color="red";
    }
    public void getRadius()
    {
        System.out.println("Radius is "+radius);
    }
    public void getArea()
    {
        double a=3.14*radius*radius;
        System.out.println("Area is"+a);
    }

}

public class Circle
{
    public static void main(String args[])
    {
        CircleModel C1=new CircleModel();
        C1.getRadius();
        C1.getArea();

    }
}
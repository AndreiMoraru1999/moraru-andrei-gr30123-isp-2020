package moraru.andrei.lab2.ex5;

import java.util.Random;

public class Bubble
{
    public static void main(String args[])
    {
        Random rand = new Random();
        int n=10;
        int v[] = new int[n];
        for (int i = 0; i < n; i++) {
            v[i] = rand.nextInt(20);
        }

        int aux = 0;
        for(int i=0; i < n; i++)
        {
            for(int j=1; j < (n-i); j++)
            {
                if(v[j-1] > v[j])
                {
                    aux = v[j-1];
                    v[j-1] = v[j];
                    v[j] = aux;
                }
            }
        }


        for(int i=0; i<n; i++)
        {
            System.out.print(v[i] + " ");
        }

    }
}